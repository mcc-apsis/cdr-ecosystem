import json
import pandas as pd
from pathlib import Path

import logging

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('notebook')
logger.setLevel(logging.DEBUG)

BASE = Path('../data/')

FILES = [
    (BASE / 'scs/articles.jsonl', BASE / 'scs/articles.arrow', BASE / 'scs/articles.csv'),
    (BASE / 'beccs/articles.jsonl', BASE / 'beccs/articles.arrow', BASE / 'beccs/articles.csv'),
]


def read_line(line: str):
    art = json.loads(line)
    art['authorships'] = json.loads(art['authorships']) if art.get('authorships') is not None else None
    art['indexed_in'] = json.loads(art['indexed_in'][0]) if art.get('indexed_in') is not None else None
    art['locations'] = json.loads(art['locations']) if art.get('locations') is not None else None
    return art


for src, dest_arrow, dest_csv in FILES:
    logger.info(f'Reading {src}')
    with open(src, 'r') as f:
        articles = [read_line(f_line) for f_line in f]

    logger.info(f'Writing {dest_arrow}')
    pd.DataFrame(articles).to_feather(dest_arrow)

    logger.info(f'Writing {dest_csv}')
    pd.DataFrame(articles).to_csv(dest_csv, index=False)

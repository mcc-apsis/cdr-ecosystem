import httpx
import pandas as pd
from pathlib import Path

import logging

BASE = Path('../data/')

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s',
                    level=logging.INFO, filename=BASE / 'pdfs.log')
logger = logging.getLogger('notebook')
logger.setLevel(logging.DEBUG)

FILES = [
    (BASE / 'scs/articles.arrow', BASE / 'scs/pdfs/'),
    (BASE / 'beccs/articles.arrow', BASE / 'beccs/pdfs/'),
]

for src, tgt in FILES:
    logger.info(f'Downloading {src} to {tgt}')
    df = pd.read_feather(src)
    tgt.mkdir(parents=True, exist_ok=True)

    for i, row in df.iterrows():
        logger.info(f'Retrieving PDF for {i}/{len(df)} ({row['id']})')
        if row['locations'] is not None:
            for loc in row['locations']:
                pdf_name = tgt / f'{row['id']}.pdf'
                if pdf_name.exists():
                    logger.debug(f'Skipping, {pdf_name} already exists.')
                if loc['pdf_url'] is not None:
                    logger.debug(f'Attempting to download {row['id']} via {loc["pdf_url"]}')
                    try:
                        r = httpx.get(loc['pdf_url'], timeout=60, verify=False, follow_redirects=True)
                        if r.status_code == 200:
                            with open(pdf_name, 'wb') as f:
                                f.write(r.content)
                            continue
                        else:
                            logger.warning(f'Location for {row["id"]} could not be downloaded')
                    except Exception as e:
                        logger.exception(e)
                        # raise e


# Download from 01.07.2024:
# SCS: 1646 / 5,000
# BECCS: 833 / 2,509

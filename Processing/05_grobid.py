import httpx
import pandas as pd
from pathlib import Path
from grobid_client.grobid_client import GrobidClient

import logging

BASE = Path('../data/')

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s',
                    level=logging.INFO, filename=BASE / 'pdfs.log')
logger = logging.getLogger('notebook')
logger.setLevel(logging.DEBUG)

FILES = [
    (BASE / 'scs/pdfs/', BASE / 'scs/teis/'),
    (BASE / 'beccs/pdfs/', BASE / 'beccs/teis/'),
]

client = GrobidClient(config_path='05_grobid.config.json')
for src, tgt in FILES:
    logger.info(src)
    logger.info(tgt)
    client.process(service='processFulltextDocument',
                   input_path=src,  # input directory
                   output=tgt,  # output directory
                   n=10,  # thread pool size
                   segment_sentences=True,
                   verbose=True)

# Parsed download from 01.07.2024:
# SCS: 1,579 / 1,646 / 5,000
# BECCS: 775 / 833 / 2,509
# (successful grobid / successful download / total abstracts)

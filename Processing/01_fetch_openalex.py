import asyncio
import json
from pathlib import Path

import httpx
import logging

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('notebook')
logger.setLevel(logging.DEBUG)

BASE = Path('../data/')

QUERIES = [
    (BASE / 'scs/articles.jsonl', '''
    (
      "soil carbon management" OR
      "soil carbon sequestration" OR
      (
        (
          "soil organic carbon" OR
          {!surround v='(soil 3N (carbon OR CO2) 3N
            (sequestration OR sequestrations OR sequestered OR sequester OR sequesters OR sequestrated OR
            storage OR
            removal OR removals OR removed OR removing OR remove OR removes OR removable))'}
        ) AND (
          biochar or bio-char
        )
      )
    ) OR (
      (
        {!surround v='(
          soil 3N (carbon OR CO2) 3N
          (
            sequestration OR sequestrations OR sequestered OR sequester OR sequesters OR sequestrated OR
            storage OR removal OR removals OR removed OR removing OR remove OR removes OR removable
          )
        )'}
      ) AND (
        "climate change" OR "global warming"
      ) AND (
        management OR managements OR manage OR manages OR managed OR practice OR practices
        OR practiced OR restoration OR land-use
      )
    ) OR (
      "carbon farming" 
      OR 
      (
        "land management" AND
        {!surround v='(carbon OR CO2) 3N
        (sequestration OR sequestrations OR sequestered OR sequester OR sequesters OR sequestrated OR
        storage OR removal OR removals OR removed OR removing OR remove OR removes OR removable)'}
      )
    )
    '''),
    (BASE / 'beccs/articles.jsonl', '''
    (
      BECCS OR 
      (
        (biomass OR bioenergy)
        AND 
        (CCS OR "carbon capture and storage" OR "Carbon dioxide capture and storage" OR "CO2 capture and storage")
      )
    )
    ''')
]


async def fetch_data(query: str, dst: Path, fq: str | None = None):
    params = {
        'q': query,
        'q.op': 'AND',
        'df': 'title_abstract',
        'useParams': '',
        'defType': 'lucene',
        'rows': 500,
        'fq': fq,
        'sort': 'id desc',
        'cursorMark': '*'
    }
    dst.parent.mkdir(parents=True, exist_ok=True)
    with open(dst, 'w') as f:
        async with httpx.AsyncClient() as client:
            batch_i = 0
            num_docs_cum = 0
            while True:
                batch_i += 1
                logger.info(f'Running query for batch {batch_i} with cursor "{params["cursorMark"]}"')
                request = await client.post(f'http://srv-mcc-apsis-rechner:8983/solr/openalex/select', data=params,
                                            timeout=60)
                response = request.json()

                next_curser = response.get('nextCursorMark')
                params['cursorMark'] = next_curser
                n_docs_total = response['response']['numFound']
                batch_docs = response['response']['docs']
                n_docs_batch = len(batch_docs)
                num_docs_cum += n_docs_batch

                if n_docs_total > 0:
                    logger.debug(
                        f'Current progress: {num_docs_cum:,}/{n_docs_total:,}={num_docs_cum / n_docs_total:.2%} docs')

                if len(batch_docs) == 0:
                    logger.info('No documents in this batch, assuming to be done!')
                    break

                if next_curser is None:
                    logger.info('Did not receive a `nextCursorMark`, assuming to be done!')
                    break

                logger.debug(f'Received response from OpenAlex: {response["responseHeader"]}')

                for doc in response['response']['docs']:
                    f.write(json.dumps(doc) + '\n')


async def main():
    for path, q in QUERIES:
        await fetch_data(query=q, dst=path)


if __name__ == '__main__':
    asyncio.run(main())

SELECT *
FROM openalex.concepts
WHERE display_name ILIKE '%climat%';

-- OpenAlex concepts (first three levels combined)
SELECT c0.display_name, c1.display_name, c2.display_name, c2.cited_by_count, c2.description, c1.description, c0.description
FROM openalex.concepts c2
         LEFT JOIN openalex.concepts_ancestors a21 ON a21.child_concept_id = substr(c2.concept_id, 22)
         LEFT JOIN openalex.concepts c1 ON substr(c1.concept_id, 22) = a21.parent_concept_id
         LEFT JOIN openalex.concepts_ancestors a10 ON a10.child_concept_id = substr(c1.concept_id, 22)
         LEFT JOIN openalex.concepts c0 ON substr(c0.concept_id, 22) = a10.parent_concept_id
WHERE c2.level = 2
  AND c2.cited_by_count > 1000000
ORDER BY c0.display_name, c1.display_name, c2.cited_by_count;
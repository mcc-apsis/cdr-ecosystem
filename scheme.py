# CDR Method
TECHNOLOGY = {
    'tech|0': {'key': 'tech|0', 'name': 'CCS', 'value': 0, 'colour': (5.54, 74.80, 48.24)},
    'tech|1': {'key': 'tech|1', 'name': 'BECCS', 'value': 1, 'colour': (27.53, 97.33, 70.59)},
    'tech|2': {'key': 'tech|2', 'name': 'DAC(CS)', 'value': 2, 'colour': (19.91, 89.30, 47.65)},
    'tech|3': {'key': 'tech|3', 'name': 'CCUS', 'value': 3, 'colour': (19.14, 96.45, 66.86)},
    'tech|4': {'key': 'tech|4', 'name': 'Soil Carbon Sequestration', 'value': 4, 'colour': (144.22, 100.00, 21.37)},
    'tech|5': {'key': 'tech|5', 'name': 'Afforestation/Reforestation', 'value': 5, 'colour': (138.42, 53.77, 41.57)},
    'tech|6': {'key': 'tech|6', 'name': 'Restoration of landscapes/peats', 'value': 6,
               'colour': (121.50, 40.40, 61.18)},
    'tech|7': {'key': 'tech|7', 'name': 'Agroforestry', 'value': 7, 'colour': (111.43, 47.57, 79.80)},
    'tech|8': {'key': 'tech|8', 'name': 'Forest Management', 'value': 8, 'colour': (104.51, 51.72, 94.31)},
    'tech|9': {'key': 'tech|9', 'name': 'Biochar', 'value': 9, 'colour': (336.08, 89.94, 68.82)},
    'tech|10': {'key': 'tech|10', 'name': 'Enhanced weathering (land based)', 'value': 10,
                'colour': (302.26, 32.92, 68.43)},
    'tech|11': {'key': 'tech|11', 'name': 'Ocean alkalinity enhancement', 'value': 11,
                'colour': (262.50, 30.77, 94.90)},
    'tech|12': {'key': 'tech|12', 'name': 'Blue carbon', 'value': 12, 'colour': (220.00, 37.50, 81.18)},
    'tech|13': {'key': 'tech|13', 'name': 'Algae farming', 'value': 13, 'colour': (143.33, 17.31, 59.22)},
    'tech|14': {'key': 'tech|14', 'name': 'Ocean fertilization & Artificial upwelling', 'value': 14,
                'colour': (202.46, 94.48, 35.49)},
    'tech|15': {'key': 'tech|15', 'name': 'General Literature on CDR', 'value': 15, 'colour': (0.00, 0.00, 38.82)},
    'tech|16': {'key': 'tech|16', 'name': 'Other technologies', 'value': 16, 'colour': (0.00, 0.00, 74.12)},
}

# Scientific method
METHOD = {
    'meth|0': {'key': 'meth|0', 'name': 'Experimental - fieldstudy', 'value': 0, 'colour': (213.96, 42.40, 75.49)},
    'meth|1': {'key': 'meth|1', 'name': 'Experimental - laboratory', 'value': 1, 'colour': (184.32, 69.06, 35.49)},
    'meth|2': {'key': 'meth|2', 'name': 'Modelling', 'value': 2, 'colour': (338.33, 100.00, 92.94)},
    'meth|3': {'key': 'meth|3', 'name': 'Data analysis / statistical analysis / econometrics', 'value': 3,
               'colour': (25.18, 97.97, 61.37)},
    'meth|4': {'key': 'meth|4', 'name': 'Life cycle assessment', 'value': 4, 'colour': (19.44, 99.08, 42.75)},
    'meth|5': {'key': 'meth|5', 'name': 'Review', 'value': 5, 'colour': (19.29, 93.33, 94.12)},
    'meth|6': {'key': 'meth|6', 'name': 'Systematic review', 'value': 6, 'colour': (355.77, 89.87, 84.51)},
    'meth|7': {'key': 'meth|7', 'name': 'Survey', 'value': 7, 'colour': (336.08, 89.94, 68.82)},
    'meth|8': {'key': 'meth|8', 'name': 'Qualitative research', 'value': 8, 'colour': (316.65, 98.86, 34.31)},
    'meth|9': {'key': 'meth|9', 'name': 'Unknown method', 'value': 9, 'colour': (0.00, 0.00, 80.00)},
}

# Main focus of study
FOCUS = {
    'cont|0': {'key': 'cont|0', 'name': 'Earth system', 'value': 0, 'colour': (359.40, 79.45, 49.61)},
    'cont|1': {'key': 'cont|1', 'name': 'Equity and ethics', 'value': 1, 'colour': (0.61, 92.45, 79.22)},
    'cont|2': {'key': 'cont|2', 'name': 'Policy/government', 'value': 2, 'colour': (116.38, 56.86, 40.00)},
    'cont|3': {'key': 'cont|3', 'name': 'Public perception', 'value': 3, 'colour': (91.76, 57.05, 70.78)},
    'cont|4': {'key': 'cont|4', 'name': 'Socio-economic pathways', 'value': 4, 'colour': (204.16, 70.62, 41.37)},
    'cont|5': {'key': 'cont|5', 'name': 'Technology', 'value': 5, 'colour': (200.66, 52.14, 77.06)},
}

-- Drop open assignments from scope
DELETE
FROM assignment
WHERE assignment_id IN (SELECT assignment.assignment_id
                        FROM assignment
                                 LEFT JOIN annotation a ON assignment.assignment_id = a.assignment_id
                        WHERE assignment_scope_id = 'c0b4cad3-0686-4239-8fa7-db7a1f65d168'
                          and a.item_id IS NULL);
LABELS = {
    'Technology': {
        'BECCS': {
            'name': 'Bioenergy with Carbon Capture and Storage (BECCS)',
            'keywords': [
                ('Bioenergy with Carbon Capture and Storage',),
                ('BECCS',),
                ('Carbon negative biofuel',),
                ('CCS', 'BECCS',),
                ('CCS', 'Bioenergy',),
            ]
        },
        'Biomass feedstocks': {
            'name': 'Biomass feedstocks',
            'keywords': [
                ('Biomass',),
                ('Forest biomass',),
                ('Forest residues',),
                ('Energy crops',),
                ('Agricultural residues',),
                ('Industrial residues',),
                ('Biowaste',), ('biogenic waste',),
                ('Animal Manure',),
                ('Algae',),
                ('Municipal solid waste; MSW',),
                ('Sewage sludge',),
                ('Paludiculture',),
                ('Biomass from land remediation activities',),
                ('Biomass from landscape management',),
            ]
        },
        'Biomass supply chain': {
            'name': 'Biomass supply chain',
            'keywords': [
                ('Biomass', 'production',),
                ('Biomass', 'collection',),
                ('Harvest',),
                ('Biomass', 'processing',),
                ('Biofuel',),
                ('Intermediate',),
                ('Conversion',),
                ('Plant site',),
                ('Bioenergy plant',),
                ('Distribution',),
                ('Transport',),
                ('Producer',),
                ('Customer',),
            ]
        },
        'Conversion pathway': {
            'name': 'Biomass/BECCS conversion pathway',
            'keywords': [
                ('Biodiesel',),
                ('Ethanol',),
                ('Fischer–Tropsch',),
                ('Hydrogen',),
                ('Thermochemical conversion',),
                ('Power and heat',),
                ('Pulverised fuel power generation',),
                ('Combined heat and power',),
                ('Energy from waste',),
                ('EfW',),
                ('WtE',),
                ('Biogas and Biomethane',),
                ('Pulp and Paper',),
                ('Iron and Steel',),
                ('Cement',),
            ]
        }
    },
    'Scientific method': {
        'Life cycle assessment': {
            'name': 'Life cycle assessment (LCA)',
            'keywords': [
                ('Life Cycle Assessments',),
                ('LCA',),
                ('Life Cycle inventory',),
                ('Life Cycle Sustainability Assessment',),
                ('LCSA',),
            ]
        },
        'Modelling': {
            'name': 'Modelling',
            'keywords': [
                ('Technoeconomic assessment',),
                ('TEA',),
                ('IAM',),
                ('Integrated assessment model',),
                ('Simulation',),
                ('Modelling',),
                ('Process modelling',),
                ('Economic assessment',),
                ('Scenarios',),
                ('Uncertainties',),
            ]
        },
        'Experiment': {
            'name': 'Experiment',
            'keywords': [
                ('Experiment',),
                ('Experimental',),
            ]
        },
        'Field study': {
            'name': 'Field study',
            'keywords': [
                ('Observational study',),
                ('Mesocosm experiment',),
                ('Field study',),
            ]
        },
        'Laboratory experiments': {
            'name': 'Laboratory experiments',
            'keywords': [
                ('Laboratory experiment',),
            ]
        },
        'Review': {
            'name': 'Review',
            'keywords': [
                ('Overview',),
                ('Perspective',),
                ('Review',),
            ]
        },
        'Systematic review': {
            'name': 'Systematic review',
            'keywords': [
                ('Systematic review',),
                ('Meta analysis',),
                ('Meta-analysis',),
            ]
        },
        'Survey': {
            'name': 'Survey',
            'keywords': [
                ('Survey',),
                ('Expert interview',),
                ('Subject Matter Expert',),
                ('SME',),
            ]

        },
        'Analysis': {
            'name': 'Data analysis / Statistical analysis / Econometric',
            'keywords': [
                ('Meta analysis',),
                ('Machine learning approaches',),
                ('Multi-criteria analysis',),
                ('Statistical linear-mixed effect models',),
            ]
        },
        'Qualitative research': {
            'name': 'Qualitative research',
            'keywords': [
                ('Focus group',),
                ('Case studies',),
                ('Framework development',),
            ]
        }
    },
    'Main focus': {
        'Perceptions': {
            'name': 'Public perception & acceptance',
            'keywords': [
                ('Public perception',),
                ('Public acceptance',),
                ('Social study',),
            ]
        },
        'Governance': {
            'name': 'Policy, politics and governance',
            'keywords': [
                ('Policy',),
                ('Governance',),
                ('Synergies',),
                ('Trade-offs',), ]
        },
        'Systems': {
            'name': 'Earth system/ Climate Systems',
            'keywords': [
                ('Earth systems', 'modelling',),
                ('Climate systems', 'modelling',),
                ('MAgPIE',),
                ('LPJ',),
                ('LPJmL',),
                ('JSBACH',),
            ]
        },
        'Pathways': {
            'name': 'Socio-economic pathways',
            'keywords': [
                ('Market modelling',),
                ('Emission pathways',),
                ('Scenario modelling',),
                ('Energy system portfolios',),
                ('Integrated assessments models',),
                ('IAM',),
                ('Government investments',),
                ('Land-use',),
            ]
        },
        'Technology': {
            'name': 'Technology',
            'keywords': [
                ('Technology readiness level',),
                ('TRL',),
                ('Technology landscape',),
            ]
        },
        'Equity': {
            'name': 'Equity & Ethics',
            'keywords': [
                ('Risk assessment',),
                ('Fair transition',),
                ('Burden-sharing',),
            ]
        },
        'MRV': {
            'name': 'Monitoring Reporting and Verification (MRV)',
            'keywords': [
                ('Monitoring', 'reporting', 'verification',),
                ('MRV',),
                ('MRV protocols',),
                ('MRV frameworks',),
                ('MRV technologies',),
            ]
        },
        'Scale-up': {
            'name': 'Scale-up',
            'keywords': [
                ('Scale up',),
            ]
        }
    },
    'Assessment': {
        'Economic considerations': {
            'name': '',
            'keywords': [
                ('Technoeconomic assessment',),
                ('TEA',),
                ('Cost',),
                ('CAPEX',),
                ('OPEX',),
                ('Social cost',),
                ('Total cost',),
                ('Levelized cost of electricity',),
                ('LCOE',),
            ]
        },
        'Potentials': {
            'name': 'Storage/sequestration potentials',
            'keywords': [
                ('Storage potential',),
                ('Removal capacity',),
                ('Sequestration potential',),
            ]
        },
        'Synergies': {
            'name': 'Synergies and system integration',
            'keywords': [
                ('System integration models',),
                ('Power',),
                ('Transport',),
            ]
        },
        'Environmental side-effects': {
            'name': 'Environmental side effects',
            'keywords': [
                ('Environmental co-benefits',),
                ('Environmental trade-offs',),
                ('Environmental side effects',),
                ('Land competition',),
                ('Land use',),
                ('Water use',),
            ]
        },
        'Non-environmental side-effects': {
            'name': 'Non-environmental side-effects',
            'keywords': [
                ('Co-benefits',),
                ('Trade-offs',),
            ]
        },
        'Ex-post': {
            'name': 'Ex-post',
            'keywords': [
                ('Ex-post',),
            ]
        },
        'Ex-ante': {
            'name': 'Ex-ante',
            'keywords': [
                ('Ex-ante',)
            ]
        },
    }
}

if __name__ == '__main__':
    for k, v in LABELS.items():
        print(k)
        print('|'.join(set([w.strip().lower()
                            for vi in v.values()
                            for kw in vi['keywords']
                            for kwi in kw
                            for w in kwi.split(' ')])))

import json
from collections import Counter
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt

plt.figure(figsize=(14, 10), dpi=120)
m = Basemap(projection='mill', resolution='c')
m.drawcoastlines()
m.drawcountries()
# m.drawstates()
m.fillcontinents(color='#eeeeee', lake_color='#FFFFFF')
m.drawmapboundary(fill_color='#FFFFFF')

locs = []
countries = []
cnt_paps = 0
with open('data/beccs/geocodes.jsonl', 'r') as f:
    for line in f:
        cnt_paps += 1
        obj = json.loads(line)
        for place in obj['places']:
            locs.append((place['lat'], place['lon']))
            countries.append(place['country_code3'])

print('num papers with geoname', cnt_paps)
locs_n = Counter(locs)
print('locs', len(locs))
print('locs', len(set(locs)))
loc_counts = locs_n.most_common()

print(loc_counts[0])

print(Counter(countries).most_common())

MAX = loc_counts[0][1]

for (lat, lon), cnt in loc_counts:
    x, y = m(lon, lat)
    m.plot(x, y, 'ro', ms=((cnt / MAX) * 9.5) + 0.5)

plt.title("Mentioned locations in abstract")
plt.tight_layout()
plt.show()
